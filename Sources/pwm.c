/*
 * pwm.c
 *
 *  Created on: 21/11/2016
 *      Author: Otavio
 */

#include "MKE04Z4.h"

#include "pwm.h"

void PWM_Start(void){
	SIM->SCGC |= SIM_SCGC_FTM0_MASK;
}
void PWM_Stop(void){
	SIM->SCGC &= ~(SIM_SCGC_FTM0_MASK);
}

uint8_t PWM_Init(uint8_t pin){
	// check if valid parameter
	if((pin < 1) || (pin > 4))
		return 0;

	// disable write protection
	if((FTM0->FMS & FTM_FMS_WPEN_MASK))
		FTM0->MODE |= FTM_MODE_WPDIS_MASK;

	// wait for WPEN to be cleared
	while(FTM0->FMS & FTM_FMS_WPEN_MASK);

	// prescaler output of 5.242MHz
	FTM0->SC = (FTM_SC_CLKS(0x01) | FTM_SC_PS(0x00));

	// PWM freq of 320Hz (using full MOD range easy the duty cycle value (no need for division)
	FTM0->MOD = 0xffff;

	// setup pin muxing
	SIM->PINSEL &= ~(SIM_PINSEL_FTM0PS0_MASK | SIM_PINSEL_FTM0PS1_MASK);
	// ftm bits only need to be set for port B
	if((pin == PWM_CH0_PTB2) || (pin == PWM_CH1_PTB3))
		SIM->PINSEL |= (pin << 8);

	// set PWM to 0% duty cycle
	if((pin == PWM_CH0_PTA0) || (pin == PWM_CH0_PTB2))
		FTM0_C0V = FTM_CnV_VAL(0);
	else
		FTM0_C1V = FTM_CnV_VAL(0);

	return 1;
}

uint8_t PWM_setDutyCycle(uint8_t pin, uint16_t duty){
	// check if valid parameter
	if((pin < 1) || (pin > 4))
		return 0;

	// set PWM to desired duty cycle
	if((pin == PWM_CH0_PTA0) || (pin == PWM_CH0_PTB2))
		FTM0_C0V = FTM_CnV_VAL(duty);
	else
		FTM0_C1V = FTM_CnV_VAL(duty);

	return 1;
}
