/*
 * pid.h
 *
 *  Created on: 21/11/2016
 *      Author: Otavio
 */

#ifndef SOURCES_PID_H_
#define SOURCES_PID_H_

// TODO: Implement routines;

void PID_defineSetpoint(uint16_t setpoint);
uint16_t PID_CalculateOuput(uint16_t currentInputValue);

#endif /* SOURCES_PID_H_ */
