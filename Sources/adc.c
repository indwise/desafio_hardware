/*
 * analog.c
 *
 *  Created on: 01/11/2016
 *      Author: Otavio
 */

#include "MKE04Z4.h"

#include "adc.h"

void ADC_start(void){
	SIM->SCGC |= SIM_SCGC_ADC_MASK;
}
void ADC_stop(void){
	SIM->SCGC &= ~(SIM_SCGC_ADC_MASK);
}

uint8_t ADC_init(void){
	// Software selection, Vrefh/Vrefl as reference
	ADC->SC2 = 0;


	// Low power, Divide ration = 4, short sample time, 12-bit conversion, bus clock div 2 as input
	ADC->SC3 = (ADC_SC3_ADLPC_MASK | ADC_SC3_ADIV(0x3) | ADC_SC3_MODE(0x2) | ADC_SC3_ADICLK(0x01));

	// No FIFO,
	ADC->SC4 = 0;

	return 1;
}

uint8_t ADC_enableChannel(uint8_t channel){
	// Check if selected channel is valid
	if((channel == 2) || (channel == 3) || (channel > 7))
		return 0;

	// Activate ADC i/o control.
	ADC->APCTL1 |= (1 << channel);

	return 1;
}

uint8_t ADC_beginConversion(uint8_t channel, uint8_t enableIrq){
	// Valid irq selection
	if((enableIrq != ADC_ENABLE_IRQ) && (enableIrq != ADC_DISABLE_IRQ))
		return 0;

	// Check if selected channel is valid
	if((channel == 2) || (channel == 3) || (channel > 7 && channel != 0x16))
		return 0;

	ADC->SC1 = ((enableIrq << ADC_SC1_AIEN_SHIFT) | channel);

	// enable irq on NVIC
	if(enableIrq)
		NVIC_EnableIRQ(ADC0_IRQn);
	else
		NVIC_DisableIRQ(ADC0_IRQn);
}

uint8_t ADC_ConversionComplete(void){
	// Return 1 case conversion is complete, 0 otherwise.
	return ((ADC->SC1 & ADC_SC1_COCO_MASK)? 1 : 0);
}

uint16_t ADC_ReadValue(void){
	return (ADC->R & 0xFFF);
}
