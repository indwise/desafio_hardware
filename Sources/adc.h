/*
 * adc.h
 *
 *  Created on: 01/11/2016
 *      Author: Otavio
 */

#ifndef SOURCES_DRIVERS_ADC_H_
#define SOURCES_DRIVERS_ADC_H_

#define ADC_ENABLE_IRQ 1
#define ADC_DISABLE_IRQ 0

#define ADC_CHANNEL_0		0x00
#define ADC_CHANNEL_1		0x01
#define ADC_CHANNEL_4		0x04
#define ADC_CHANNEL_5		0x05
#define ADC_CHANNEL_6		0x06
#define ADC_CHANNEL_7		0x07
#define ADC_CHANNEL_TEMP	0x16


void ADC_start(void);
void ADC_stop(void);

uint8_t ADC_init(void);
uint8_t ADC_enableChannel(uint8_t channel);

uint8_t ADC_beginConversion(uint8_t channel, uint8_t enableIrq);
uint8_t ADC_ConversionComplete(void);
uint16_t ADC_ReadValue(void);

#endif /* SOURCES_DRIVERS_ADC_H_ */
