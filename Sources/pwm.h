/*
 * pwm.h
 *
 *  Created on: 21/11/2016
 *      Author: Otavio
 */

#ifndef SOURCES_PWM_H_
#define SOURCES_PWM_H_

#define PWM_CH0_PTB2 1
#define PWM_CH1_PTB3 2
#define PWM_CH0_PTA0 3
#define PWM_CH1_PTA1 4

void PWM_Start(void);
void PWM_Stop(void);

uint8_t PWM_Init(uint8_t pin);
uint8_t PWM_setDutyCycle(uint8_t pin, uint16_t duty);

#endif /* SOURCES_PWM_H_ */
