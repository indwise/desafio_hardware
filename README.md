# Vaga Desenvolvedor Firmware/Hardware #

## Descrição da vaga ##

Projeto de esquemáticos e PCB para aplicações industriais, utilizando microcontroladores ARM. Desenvolvimento de firmwares novos ou modificações de projetos existentes, usando C/C++ Bare metal ou RTOS.

Criação e montagem de painéis elétricos para uso em ambiente industrial. Manutenção e reparo em equipamentos eletrônicos.

## Pré-requisitos ##

* C/C++;
* Formação em Engenharia Elétrica ou afins (conclusão do curso entre 2014/2017);
* Experiencia com microcontroladores;

## Conhecimentos Desejáveis ##

* Projeto de esquemáticos/PCB utilizando Eagle, Altium ou Programa CAD similar;
* Experiência com microcontroladores ARM;

# Desafio Técnico #

Para testar as habilidades do candidato publicamos um pequeno desafio. O resultado final deve ser um conjunto de PCB/Esquemático/Firmware que seja capaz de realizar leituras de temperatura de um sensor e, através de um bloco de controle PID, controlar a potência de um ventilador utilizando um sinal PWM. O conjunto também deve capacidades de comunicação UART, o protocolo de comunicação utilizado deve ser documentado).

Os arquivos gerados devem ser publicados em um repositório do bitbucket.

Arquivos de exemplo foram publicados nesse repositório e podem ser usados como referência. No exemplo utilizou-se o microcontrolador NXP MKE04Z8VTG4 de arquitetura ARM. O candidato pode, no entanto, utilizar qualquer arquitetura e fabricante que desejar, desde que não sejam utilizadas placas de desenvolvimento, somente o IC escolhido (exemplo não utilizar Arduino UNO mas o microcontrolador ATMEGA328P).

Seguem as especificações para o projeto:

## Desenvolvimento Hardware ##

Utilizando uma ferramenta CAD de desenvolvimento de PCB (a CADSoft fornece uma versão gratuita do EAGLE que pode ser utilizado: [EAGLE - CADSoft](https://cadsoft.io/)) projete o esquemático e desenho de PCB de um circuito com as seguintes especificações: 

* Alimentação externa de 5V através de um conector [AK500](http://www.eletrodex.com.br/borne-2-conexoes-azul.html);
* Realizar a leitura de sensor analógico de temperatura (especificar no esquemático sensor utilizado);
* Controlar acionamento de um ventilador usando sinal PWM (2A@5V em alimentação DC);
* Conector de saída utilizando UART (conector utilizado e níveis de tensão devem ser documentados);
* Pinout para gravação do microcontrolador (a referencia de programador pode ser uma placa de desenvolvimento do microcontrolador alvo, porém o pinout e forma de conexão deve ser documentado);

## Desenvolvimento Firmware ##

Junto das especificações de hardware deve ser entregue o código-fonte para o controlador alvo capaz de:

* Realizar a leitura analógica do sensor de temperatura;
* Controlar o duty cycle de um sinal PWM para que a temperatura seja mantida em 30C (um duty cycle 50% tende a manter a temperatura lida estável);
* Capacidade de comunicação com monitoramento externo através da interface UART (pré-requisitos da comunicação estão especificados na próxima sessão);

Esse repositório também contém um código-fonte implementado, para o alvo MKE04Z8VTG4, como exemplo. No código estão presentes drivers para periféricos de PWM e ADC do controlador. Foi escolhido no exemplo um controle da temperatura através de um bloco PID (o site embarcados contém uma descrição e código exemplo de um bloco PID: [https://www.embarcados.com.br/controle-pid-em-sistemas-embarcados/](https://www.embarcados.com.br/controle-pid-em-sistemas-embarcados/)) porém o candidato pode escolher a abordagem de controle que preferir.

## Pré-requisitos comunicação UART ##

O monitor externo deve, utilizando UART, poder receber as seguintes informações do microcontrolador:

* Temperatura atual informada pelo sensor em graus;
* Duty cycle atual do ventilador em porcentagem;

O protocolo de comunicação utilizado deve ser documentado e publicado na wiki do repositório.